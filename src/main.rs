use std::{error::Error, fmt::Display, fs, io::{ErrorKind, Read, Seek, SeekFrom, Write}, path::Path, process::Command};

use clap::{App, Arg};
use fs::{File, OpenOptions};

#[derive(Debug)]
struct AppError(&'static str);
impl Display for AppError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.0)
  }
}
impl Error for AppError {}

fn main() -> Result<(), Box<dyn Error>> {
  let matches = App::new("batch-find-replace")
    .arg(Arg::with_name("original-list").index(1).required(true))
    .arg(Arg::with_name("edited-list").index(2).required(true))
    .arg(Arg::with_name("file-list").index(3).required(true))
    .get_matches();
  let orig_list_str = fs::read_to_string(matches.value_of_os("original-list").unwrap())?;
  let edited_list_str = fs::read_to_string(matches.value_of("edited-list").unwrap())?;
  let orig_list = orig_list_str.lines().collect::<Vec<_>>();
  let edited_list = edited_list_str.lines().collect::<Vec<_>>();
  if orig_list.len() != edited_list.len() {
    return Err(Box::new(AppError("Two provided files does not have the same number of lines.")));
  }
  let file_list_str = fs::read_to_string(matches.value_of_os("file-list").unwrap())?;
  let file_list = file_list_str.lines().collect::<Vec<_>>();

  for fp in file_list {
    let mut file = OpenOptions::new().read(true).write(true).open(fp)?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    for (from, replace) in orig_list.iter().copied().zip(edited_list.iter().copied()) {
      if from == replace {
        continue;
      }
      if from.is_empty() || replace.is_empty() {
        return Err(Box::new(AppError("Invalid line: one is empty but the other is not.")));
      }
      content = content.replace(from, replace);
    }
    file.seek(SeekFrom::Start(0))?;
    file.set_len(content.as_bytes().len() as u64)?;
    file.write(content.as_bytes())?;
    println!("{}", fp);
  }

  Ok(())
}
